package com.example;

import com.example.service.HelloServiceImpl;
import com.example.thrift.api.HelloService;
import lombok.extern.slf4j.Slf4j;

import org.apache.log4j.Logger;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by henry on 2018/7/23.
 */
@Slf4j
@SpringBootApplication
public class Application {
    private static final Logger LOGGER = Logger.getLogger(Application.class);
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

        try {
            TProcessor tprocessor = new HelloService.Processor<>(new HelloServiceImpl());
            TServerSocket serverTransport = new TServerSocket(9898);
            TServer.Args tArgs = new TServer.Args(serverTransport);
            tArgs.processor(tprocessor);
            tArgs.protocolFactory(new TBinaryProtocol.Factory());
            TServer server = new TSimpleServer(tArgs);
            server.serve();
            LOGGER.info("Servidor Iniciado....");
        } catch (TTransportException e) {
            LOGGER.error("Stop Server", e);
        }
    }
}
